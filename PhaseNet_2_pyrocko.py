import argparse
import pandas as pd

def convert(picks_file_name,output_markers_name):
    df_test = pd.read_csv(picks_file_name)
    
    with open(output_markers_name,'w') as f:
        f.write('# Snuffler Markers File Version 0.2\n')
        for index, row in df_test.iterrows():
            if row['phase_type']=='P': 
                color_pick='1'
            else:
                color_pick='2'
            f.write('phase: {:s} {:s} {:s} {:s}Z None None None {:1s} None False\n'.format(row['phase_time'][:10],row['phase_time'][11:], color_pick, row['station_id'],row['phase_type']))
        print('Conversion done. File "{}" written to disk.'.format(output_markers_name))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert PhaseNet picks to Pyrocko markers")
    parser.add_argument('-f','--file', action='store', type=str, required=True, help='PhaseNet picks file in csv format.')
    parser.add_argument('-o','--output', action='store', type=str, required=False, help='Pyrocko markers file.', default='PhaseNet_markers.markers')
    args = parser.parse_args()
    convert(args.file, args.output)
